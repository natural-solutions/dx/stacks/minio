#!/bin/bash

set -e

# Pre-pre-flight? 🤷
if [[ -n "$MSYSTEM" ]]; then
    echo "Seems like you are using an MSYS2-based system (such as Git Bash) which is not supported. Please use WSL instead.";
    exit 1
fi

source ./docker/.env

docker-compose --project-name=${PROJECT_NAME} --project-directory=./docker "$@"
